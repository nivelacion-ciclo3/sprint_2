-- phpMyAdmin SQL Dump
-- version 4.0.4.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 29-09-2022 a las 02:37:13
-- Versión del servidor: 5.6.13
-- Versión de PHP: 5.4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `fruverlife`
--
CREATE DATABASE IF NOT EXISTS `fruverlife` DEFAULT CHARACTER SET utf32 COLLATE utf32_spanish_ci;
USE `fruverlife`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alarmafinal`
--

CREATE TABLE IF NOT EXISTS `alarmafinal` (
  `fechaalarmault` date NOT NULL,
  `codigousuario` int(10) NOT NULL,
  `codigofruver` int(10) NOT NULL,
  `fechacompra` int(10) NOT NULL,
  `alarmafinal` varchar(30) COLLATE utf32_spanish2_ci NOT NULL,
  `fechaalarma` date NOT NULL,
  PRIMARY KEY (`fechaalarmault`),
  UNIQUE KEY `fechaalarmault` (`fechaalarmault`),
  KEY `codigofruver` (`codigofruver`),
  KEY `codigousuario` (`codigousuario`),
  KEY `fechacompra` (`fechacompra`),
  KEY `fechaalarma` (`fechaalarma`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alarmatemprana`
--

CREATE TABLE IF NOT EXISTS `alarmatemprana` (
  `fechaalarma` date NOT NULL,
  `codigousuario` int(10) NOT NULL,
  `codigofruver` int(10) NOT NULL,
  `fechacompra` date NOT NULL,
  PRIMARY KEY (`fechaalarma`),
  KEY `codigofruver` (`codigofruver`),
  KEY `fechacompra` (`fechacompra`),
  KEY `codigousuario` (`codigousuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `almacenamiento`
--

CREATE TABLE IF NOT EXISTS `almacenamiento` (
  `codigofruver` int(10) NOT NULL,
  `fechacompra` date NOT NULL,
  `fechaalarma` date NOT NULL,
  PRIMARY KEY (`codigofruver`),
  KEY `fechacompra` (`fechacompra`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inventario`
--

CREATE TABLE IF NOT EXISTS `inventario` (
  `codigofruver` int(10) NOT NULL,
  `codigousuario` int(10) NOT NULL,
  `fechacompra` date NOT NULL,
  PRIMARY KEY (`codigofruver`),
  KEY `fechacompra` (`fechacompra`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `codigousuario` int(10) NOT NULL,
  `codigofruver` int(10) NOT NULL,
  `fechacompra` date NOT NULL,
  PRIMARY KEY (`codigofruver`),
  KEY `fechacompra` (`fechacompra`),
  KEY `fechacompra_2` (`fechacompra`),
  KEY `fechacompra_3` (`fechacompra`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_spanish_ci;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alarmafinal`
--
ALTER TABLE `alarmafinal`
  ADD CONSTRAINT `alarmafinal_ibfk_4` FOREIGN KEY (`fechaalarmault`) REFERENCES `menu` (`fechacompra`),
  ADD CONSTRAINT `alarmafinal_ibfk_1` FOREIGN KEY (`codigousuario`) REFERENCES `alarmatemprana` (`codigousuario`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `alarmafinal_ibfk_2` FOREIGN KEY (`codigofruver`) REFERENCES `alarmatemprana` (`codigofruver`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `alarmafinal_ibfk_3` FOREIGN KEY (`fechaalarma`) REFERENCES `almacenamiento` (`fechacompra`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `alarmatemprana`
--
ALTER TABLE `alarmatemprana`
  ADD CONSTRAINT `alarmatemprana_ibfk_3` FOREIGN KEY (`codigofruver`) REFERENCES `alarmafinal` (`codigofruver`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `alarmatemprana_ibfk_1` FOREIGN KEY (`fechaalarma`) REFERENCES `alarmafinal` (`fechaalarmault`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `alarmatemprana_ibfk_2` FOREIGN KEY (`codigousuario`) REFERENCES `alarmafinal` (`codigousuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `almacenamiento`
--
ALTER TABLE `almacenamiento`
  ADD CONSTRAINT `almacenamiento_ibfk_2` FOREIGN KEY (`fechacompra`) REFERENCES `alarmatemprana` (`fechacompra`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `almacenamiento_ibfk_1` FOREIGN KEY (`codigofruver`) REFERENCES `alarmatemprana` (`codigofruver`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `inventario`
--
ALTER TABLE `inventario`
  ADD CONSTRAINT `inventario_ibfk_2` FOREIGN KEY (`fechacompra`) REFERENCES `almacenamiento` (`fechacompra`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `inventario_ibfk_1` FOREIGN KEY (`codigofruver`) REFERENCES `almacenamiento` (`codigofruver`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`fechacompra`) REFERENCES `almacenamiento` (`fechacompra`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `menu_ibfk_2` FOREIGN KEY (`codigofruver`) REFERENCES `almacenamiento` (`codigofruver`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
